"use strict";

const rootBox = document.createElement('div');
document.body.append(rootBox);

function getFilmList() {
    fetch('https://ajax.test-danit.com/api/swapi/films')
    .then((response) => response.json()).then((data) => {
        console.log(data);
        data.forEach(filmInfoObj => {
            const {id, name, openingCrawl, characters} = filmInfoObj;
            const elems = {
                container: document.createElement('div'),
                episode: document.createElement('h3'),
                title: document.createElement('h4'),
                summary: document.createElement('p'),
                charactersBox: document.createElement('ul'),
            }   
            elems.episode.textContent = `Episode: ${id}`;
            elems.title.textContent = `"${name}"`;
            elems.summary.textContent = `Summary: ${openingCrawl}`; 
            elems.container.append(elems.episode, elems.title, elems.summary);
            rootBox.append(elems.container);  
            
            characters.forEach(character => {
                fetch(character).then((response) => response.json()).then((data) => {
                    const characterName = document.createElement('li');
                    characterName.textContent = data.name;
                    elems.charactersBox.append(characterName);
                    elems.title.after(elems.charactersBox);
                });
            });        
        });  
    });
};

getFilmList();